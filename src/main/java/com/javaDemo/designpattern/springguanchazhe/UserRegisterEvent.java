package com.javaDemo.designpattern.springguanchazhe;

import org.springframework.context.ApplicationEvent;

/**
 * @ClassName: UserRegisterEvent
 * @Auther: csy
 * @Date: 2021/2/5 17:52
 * @Description:
 */
public class UserRegisterEvent extends ApplicationEvent {
    /**
     * 用户名
     */
    private String username;
    public UserRegisterEvent(Object source) {
        super(source);
    }
    public UserRegisterEvent(Object source, String username) {
        super(source);
        this.username = username;
    }
    public String getUsername() {
        return username;
    }
}