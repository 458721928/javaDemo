package com.javaDemo.designpattern.fangwenzhe;

public interface CorporateSlave {
    void accept(CorporateSlaveVisitor visitor);
}