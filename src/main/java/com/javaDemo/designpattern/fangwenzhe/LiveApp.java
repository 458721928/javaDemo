package com.javaDemo.designpattern.fangwenzhe;

public class LiveApp implements CorporateSlaveVisitor {
    @Override
    public void visit(Programmer programmer) {
        System.out.println(String.format("%s: 最近小视频很火啊，咱能不能抄袭下抖音，搞他一炮,将来公司上市了，你的身价至少也是几千万，甚至上亿...",programmer.getName()));
    }


    @Override
    public void visit(Tester tester) {
        System.out.println(String.format("%s: 你也开个账户，边测试边直播，两不耽误...",tester.getName()));
    }
}