package com.javaDemo.designpattern.daili;

public interface IOrderService {
    int saveOrder(Order order);
}