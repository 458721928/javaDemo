package com.javaDemo.designpattern.zerenlian;

/**
 * @program: cookbook
 * @description: 直接主管处理类
 * @author: Byron
 * @create: 2019/07/11 09:46
 */
public class DirectLeaderLeaveHandler extends AbstractLeaveHandler{
    public DirectLeaderLeaveHandler(String name) {
        this.handlerName = name;
    }

    protected void AbstractLeaveHandler(LeaveRequest request) {
        if(request.getLeaveDays() <= this.MIN){
            System.out.println("直接主管:" + handlerName + ",已经处理;流程结束。");
            return;
        }

        if(null != this.nextHandler){
            this.nextHandler.handlerRequest(request);
        }else{
            System.out.println("审批拒绝！");
        }

    }
    /**处理请假的请求，子类实现*/
    @Override
    protected void handlerRequest(LeaveRequest request){
        AbstractLeaveHandler(request);
    }
}