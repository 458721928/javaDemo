package com.javaDemo.designpattern.yewuceluemoshi;

public interface PermissionService {
    PermissionCheckResultDTO permissionCheck(ArtemisSellerBizType artemisSellerBizType, Long userId, String bizCode);
}
