package com.javaDemo.designpattern.yewuceluemoshi;

import org.springframework.stereotype.Component;

/**
 业务2的鉴权逻辑我们假设是这样的：
 * 趋势业务权限校验处理器
 */
@Component
public class ColdStartPermissionCheckHandlerImpl implements PermissionCheckHandler {
    @Override
    public boolean isMatched(BizType bizType) {
        return BizType.COLD_START.equals(bizType);
    }
    @Override
    public PermissionCheckResultDTO permissionCheck(Long userId, String bizCode) {
        //业务特有鉴权逻辑
        return null;
    }
}