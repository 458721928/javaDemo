package com.javaDemo.designpattern.yewuceluemoshi;

public interface PermissionCheckHandler {
    /**
     * 判断是否是自己能够处理的权限校验类型
     */
    boolean isMatched(BizType bizType);
    /**
     * 权限校验逻辑
     */
    PermissionCheckResultDTO permissionCheck(Long userId, String bizCode);
}