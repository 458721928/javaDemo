package com.javaDemo.designpattern.zhaungshizhe.cake;

public class MilkCakeDecorator extends CakeDecorator {

    public MilkCakeDecorator(Cake cake) {
        super(cake);
    }

    @Override
    public String show() {
        return "奶油 " + super.show();
    }
}
