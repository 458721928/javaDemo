package com.javaDemo.designpattern.zhaungshizhe.cake;

public class StrawberryCakeDecorator extends CakeDecorator {

    public StrawberryCakeDecorator(Cake cake) {
        super(cake);
    }

    @Override
    public String show() {
        return "草莓 " + super.show();
    }
}