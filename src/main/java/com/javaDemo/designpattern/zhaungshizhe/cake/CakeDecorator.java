package com.javaDemo.designpattern.zhaungshizhe.cake;

public abstract class CakeDecorator implements Cake {

    private Cake cake;

    public CakeDecorator(Cake cake) {
        this.cake = cake;
    }

    @Override
    public String show() {
        return this.cake.show();
    }
}