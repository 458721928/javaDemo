package com.javaDemo.designpattern.zhaungshizhe.cake;

public interface Cake {
    String show();
    public static void main(String[] args) {
        // 原味蛋糕
        Cake cake = new NormalCake();
        System.out.println(cake.show());
        // 草莓蛋糕
        cake = new StrawberryCakeDecorator(cake);
        System.out.println(cake.show());
        // 奶油草莓蛋糕
        cake = new MilkCakeDecorator(cake);
        System.out.println(cake.show());
        System.out.println("============================");
        // 原味蛋糕
        cake = new NormalCake();
        System.out.println(cake.show());
        // 奶油蛋糕
        cake = new MilkCakeDecorator(cake);
        System.out.println(cake.show());
        // 草莓奶油蛋糕
        cake = new StrawberryCakeDecorator(cake);
        System.out.println(cake.show());
    }
}









