package com.javaDemo.designpattern.zhaungshizhe;

public abstract class ABattercake {
    protected abstract String getDesc();
    protected abstract int cost();

}