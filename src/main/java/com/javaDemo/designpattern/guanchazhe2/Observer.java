package com.javaDemo.designpattern.guanchazhe2;

public interface Observer {
	public void update();
}