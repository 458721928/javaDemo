package com.javaDemo.designpattern.zhongjiezhe.jiandan;

import java.util.ArrayList;
import java.util.List;

//简单单例中介者
class SimpleMediator {
    private static final SimpleMediator smd = new SimpleMediator();
    private final List<SimpleColleague> colleagues = new ArrayList<SimpleColleague>();

    private SimpleMediator() {
    }

    public static SimpleMediator getMedium() {
        return (smd);
    }

    public void register(SimpleColleague colleague) {
        if (!colleagues.contains(colleague)) {
            colleagues.add(colleague);
        }
    }

    public void relay(SimpleColleague scl) {
        for (SimpleColleague ob : colleagues) {
            if (!ob.equals(scl)) {
                ob.receive();
            }
        }
    }
}