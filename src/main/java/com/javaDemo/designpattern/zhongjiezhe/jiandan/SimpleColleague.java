package com.javaDemo.designpattern.zhongjiezhe.jiandan;

//抽象同事类
interface SimpleColleague {
    void receive();

    void send();
}

