package com.javaDemo.designpattern.zhongjiezhe;

public interface Mediator {
	public void createMediator();
	public void workAll();
}