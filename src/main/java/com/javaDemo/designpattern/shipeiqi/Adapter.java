package com.javaDemo.designpattern.shipeiqi;

public class Adapter extends Adaptee implements Target{
    @Override
    public void request() {
        //...业务逻辑
        super.adapteeRequest();
        //...
    }
}