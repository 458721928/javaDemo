package com.javaDemo.designpattern.shipeiqi;

public class ClassadapterTest {
    public static void main(String[] args) {
        Target target = new ConcreteTarget();
        target.request();

        Target adapterTarget = new Adapter();
        adapterTarget.request();



    }
}