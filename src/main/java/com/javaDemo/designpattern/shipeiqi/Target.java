package com.javaDemo.designpattern.shipeiqi;

public interface Target {
    void request();
}