package com.javaDemo.designpattern.shipeiqi;

public class Adapter1 implements Target{
    private Adaptee adaptee = new Adaptee();

    @Override
    public void request() {
        //...
        adaptee.adapteeRequest();
        //...
    }

    public static void main(String[] args) {
        System.out.println(3|9);
    }
}