package com.javaDemo.designpattern.mingling;

public interface Command {
	public void exe();
}