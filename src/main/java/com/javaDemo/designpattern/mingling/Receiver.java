package com.javaDemo.designpattern.mingling;

public class Receiver {
	public void action(){
		System.out.println("command received!");
	}
}