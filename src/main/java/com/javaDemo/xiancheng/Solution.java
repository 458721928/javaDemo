package com.javaDemo.xiancheng;

class Solution {
    public static void main(String[] args) {
        int[] a={1,2,4,1,2};
        System.out.println(singleNumber(a));
    }
    public static int singleNumber(int[] nums) {
        Thread a=new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });
        if(nums.length < 1) {
            return 0;
        }
        int result = nums[0];
        for (int i = 1; i < nums.length; i++) {
            result = result ^ nums[i];
        }
        return result;
    }
}