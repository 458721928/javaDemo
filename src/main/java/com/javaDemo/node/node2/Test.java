package com.javaDemo.node.node2;

/**
 * @ClassName: Test
 * @Auther: csy
 * @Date: 2021/6/15 20:42
 * @Description:
 */
public class Test {
    static class Node{
        int val;
        Node next;
        public Node(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        Node node=new Node(1);
        node.next=new Node(2);
        node.next.next=new Node(3);
        rev(node);
    }

    private static Node rev(Node node) {
        if(node==null){
            return null;
        }
        Node curNode=null;
        Node resultNode=null;
        while (node!=null){
            curNode=node.next;

            node.next=resultNode;

            resultNode=node;

            node =curNode;
        }
        return resultNode;
    }

}
