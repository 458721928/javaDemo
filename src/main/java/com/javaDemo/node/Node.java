package com.javaDemo.node;

/**
 * @ClassName: Node
 * @Auther: csy
 * @Date: 2020/4/3 10:52
 * @Description:
 */
public class Node {
    private int data;
    public Node next;
    public Node(int node,Node next){
        this.data=node;
        this.next=next;
    }

    public Node() {
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node mergeLinkList(Node head1, Node head2) {
        if (head1 == null && head2 == null) {
            // 如果两个链表都为空 return null;
        }
        if (head1 == null) {
            return head2;
        }
        if (head2 == null) {
            return head1;
        }
        Node head; // 新链表的头结点
        Node current; // current结点指向新链表
        // 一开始，我们让current结点指向head1和head2中较小的数据，得到head结点
        if (head1.data < head2.data) {
            head = head1;
            current = head1;
            head1 = head1.next;
        } else {
            head = head2;
            current = head2;
            head2 = head2.next;
        }
        while (head1 != null && head2 != null) {
            if (head1.data < head2.data) {
                current.next = head1;
                // 新链表中，current指针的下一个结点对应较小的那个数据
                current = current.next; // current指针下移
                head1 = head1.next;
            } else {
                current.next = head2;
                current = current.next;
                head2 = head2.next;
            }
        }
        // 合并剩余的元素
        if (head1 != null) {
            // 说明链表2遍历完了，是空的
            current.next = head1;
        }
        if (head2 != null) {
            // 说明链表1遍历完了，是空的
            current.next = head2;
        }
        return head;
    }
    public static void main(String[] args) {
        Node list1 = new Node();
        Node list2 = new Node();
        // 向LinkList中添加数据
        for (int i = 0; i < 4; i++) {
            list1.setData(i);
        }
        for (int i = 3; i < 8; i++) {
            list2.setData(i);
        }
        Node list3 = new Node();
        list3 = list3.mergeLinkList(list1, list2); // 将list1和list2合并，存放到list3中
        // list3(list3);// 从head节点开始遍历输出
        System.out.println(111);
    }
}
