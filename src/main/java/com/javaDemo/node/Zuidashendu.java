package com.javaDemo.node;

/**
 * @ClassName: Zuidashendu
 *
 * https://leetcode-cn.com/problems/maximum-depth-of-binary-tree/solution/er-cha-shu-de-zui-da-shen-du-by-leetcode/
 * @Auther: csy
 * @Date: 2020/5/29 01:50
 * @Description:
 */
public class Zuidashendu {
    static class Solution {
        public static int maxDepth(TreeNode root) {
            if (root == null) {
                return 0;
            } else {
                int left_height = maxDepth(root.left);
                int right_height = maxDepth(root.right);
                return java.lang.Math.max(left_height, right_height) + 1;
            }
        }
    }

    static int max = 0;
    static int depth = 0;

    public static int maxDepth(TreeNode root) {
        backOrder(root);
        return max;
    }

    private static void backOrder(TreeNode root) {
        if (root != null) {
            depth++;
            max = Math.max(max, depth);
            backOrder(root.left);
            backOrder(root.right);
            depth--;
        }
    }

    public static void main(String[] args) {
        Integer[] root = {3,5,1,6};
        Zuidashendu.maxDepth(TreeNode.makeTree(root));
    }
}
