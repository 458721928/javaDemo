package com.javaDemo.shouxiexiaofeizheshenchanzhe;

import java.util.Random;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName: Producer
 * @Auther: csy
 * @Date: 2021/3/12 15:30
 * @Description:
 */
public class Producer implements Runnable {
    //共享阻塞队列
    private final BlockingDeque<Data> deque;
    //是否正在运行
    private volatile  boolean isrun = true;
    Random random=new Random();
    public Producer(BlockingDeque<Data> data) {
        deque = data;
    }


    @Override
    public void run() {
        try {
            while (isrun) {
                TimeUnit.SECONDS.sleep(10);
                // Thread.sleep(10);
                Data data=new Data(random.nextInt(10),random.nextInt(10));
                System.out.println("生产水");
                if(!deque.offer(data,2, TimeUnit.SECONDS)){
                    System.out.println("注水失败...");
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stop(){
        isrun=false;
    }
}
