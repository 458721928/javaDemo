package com.javaDemo.shouxiexiaofeizheshenchanzhe;

import java.util.Random;
import java.util.concurrent.BlockingDeque;

/**
 * @ClassName: Consumer
 * @Auther: csy
 * @Date: 2021/3/12 15:31
 * @Description:
 */
public class Consumer implements Runnable{

    private BlockingDeque<Data> deque;
    private static Random random=new Random();

    public Consumer(BlockingDeque<Data> queue){
        this.deque = queue;
    }
    @Override
    public void run() {
        while (true){
            try {
                Data data = deque.take();
                //模拟抽水耗时
                Thread.sleep(random.nextInt(1000));
                if(data != null){
                    System.out.println("当前<<抽水管:"+Thread.currentThread().getName()+",抽取水容量(L):"+data.getAge());
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }
}
