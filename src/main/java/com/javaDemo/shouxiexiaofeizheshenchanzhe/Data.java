package com.javaDemo.shouxiexiaofeizheshenchanzhe;

import lombok.AllArgsConstructor;

/**
 * @ClassName: Data
 * @Auther: csy
 * @Date: 2021/3/12 15:28
 * @Description:
 */

@lombok.Data
@AllArgsConstructor
public class Data {

    private int age;
    private int score;
}
