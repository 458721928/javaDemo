package com.javaDemo;

import lombok.SneakyThrows;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @ClassName: io
 * @Auther: csy
 * @Date: 2021/2/18 11:22
 * @Description:
 */
public class io {
    @SneakyThrows
    public static void main(String[] args) {
        Path path= Paths.get("demo.txt");
            FileWriter fileWriter=new FileWriter(path.toFile());
        fileWriter.write("aaa");
        fileWriter.flush();
        FileReader fileReader=new FileReader(path.toFile());
        InputStreamReader reader = new InputStreamReader(new FileInputStream("test.txt"),"utf-8");

        FileInputStream fs=new FileInputStream(new File("fa.txt"));
        byte[] buff=new byte[fs.available()];
        fs.read(buff);
        fs.close();
        File file;
        FileOutputStream fo=new FileOutputStream("a",true);
        fo.write(buff);
    }
}
