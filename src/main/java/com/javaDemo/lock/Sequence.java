package com.javaDemo.lock;

public class Sequence {
    private MyLock lock = new MyLock();

    public void method1() {
        lock.lock();
        System.out.println("method1");
        method2();
        lock.unlock();

    }

    public void method2() {
        lock.lock();
        System.out.println("method2");
        lock.unlock();
    }
    public static void main(String[] args) {
        Sequence s = new Sequence();
        new Thread(() -> s.method1()).start();
    }


}
