package com.javaDemo.shenchanzhehexiaofeizhe;

class Consumer implements Runnable{

    private  EventStorage storage;

    public Consumer(EventStorage eventStorage){
        storage = eventStorage;
    }


    @Override
    public void run() {
        for(int i = 0; i<100; i++){
            storage.take();
        }
    }

    public static void main(String[] args) {
        String line = ",aa,bcd,eef,,,";
        String[] split = line.split(",",2);
        System.out.println(split.length);//4

    }
}
