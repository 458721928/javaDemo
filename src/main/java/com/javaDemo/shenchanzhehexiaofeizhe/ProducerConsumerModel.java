package com.javaDemo.shenchanzhehexiaofeizhe;

/**
 *  用wait/notify实现
 */
public class ProducerConsumerModel {

    public static void main(String[] args) {
		//new出容器，
        EventStorage eventStorage = new EventStorage();
        //创建生产者消费者，并传入同一个容器
        Producer producer = new Producer(eventStorage);
        Consumer consumer = new Consumer(eventStorage);

		//启动生产者消费者
        new Thread(producer).start();;
        new Thread(consumer).start();;
    }
}
