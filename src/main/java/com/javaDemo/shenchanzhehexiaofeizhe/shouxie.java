package com.javaDemo.shenchanzhehexiaofeizhe;

import lombok.SneakyThrows;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @ClassName: shouxie
 * @Auther: csy
 * @Date: 2021/4/13 11:22
 * @Description:
 */
public class shouxie {
    static int count = 10;
    static ArrayBlockingQueue<Integer> water = new ArrayBlockingQueue<Integer>(count);
    private static volatile boolean flag = false;

    public static void main(String[] args) {
        for(int i=0;i<10;i++){
            Producer producer = new Producer();
            new Thread(producer).start();
            Consumer consumer = new Consumer();
            new Thread(consumer).start();
        }

    }

    public static class Producer implements Runnable {
        @Override
        public void run() {
            while (water.size() ==0) {
                water.offer(1);
                System.out.println("生产了一个");
            }
            flag = true;
        }
    }

    public static class Consumer implements Runnable {
        @SneakyThrows
        @Override
        public void run() {
            while (flag) {
                water.take();
                System.out.println("开始消费了一个");
                if (water.size() == 0) {
                    flag = false;
                }
            }

        }
    }
}
