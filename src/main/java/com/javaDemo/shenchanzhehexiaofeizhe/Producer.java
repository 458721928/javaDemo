package com.javaDemo.shenchanzhehexiaofeizhe;

class Producer implements Runnable{

    private  EventStorage storage;

    public Producer(EventStorage eventStorage){
        storage = eventStorage;
    }

    @Override
    public void run() {
        for(int i = 0; i<100; i++){
            storage.put();
        }
    }
}
