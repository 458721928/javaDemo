package com.javaDemo.shenchanzhehexiaofeizhe;

import java.util.Date;
import java.util.LinkedList;

class EventStorage{
	//容器大小
    private int maxSize;
    //容器
    private LinkedList<Date> storage;
	//使用构造方法初始化容器
    public EventStorage(){
        maxSize = 10;
        storage = new LinkedList<Date>();
    }

	//生产者生产产品的方法
    public synchronized void put(){
    	//当容器满了，线程就陷入等待并释放cpu
        while (storage.size() == maxSize){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        storage.add(new Date());
        System.out.println("仓库里有了"+ storage.size() +"个产品");
		//每当往容器里放入了产品就唤醒正处于等待的线程，因为除了主线程和生产者自己，就只有消费者线程，所以使用notify()就可以了，当然也可以用notifyAll()。
        notify();

    }
	//消费者消费产品的方法
    public synchronized  void take(){
    	//当容易里没有产品了就陷入等待
        while (storage.size() == 0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //LinkedList.poll拿出并删除，相当于List里的get(0)  +  remove()
        System.out.println("拿到了"+ storage.poll() + ",现在还剩下"+storage.size());
    }
}
