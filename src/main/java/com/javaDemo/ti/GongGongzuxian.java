package com.javaDemo.ti;

/**
 * @ClassName: GongGongzuxian
 * @Auther: csy
 * @Date: 2022/3/30 16:13
 * @Description:
 */
public class GongGongzuxian {

    public static class TreeNode{
        int treeNode;
        TreeNode left;
        TreeNode right;

        public TreeNode(int treeNode) {
            this.treeNode = treeNode;
            this.left = null;
            this.right = null;
        }
    }

    public static void main(String[] args) {
        TreeNode treeNode=new TreeNode(2);
        treeNode.left=new TreeNode(3);
        // treeNode.left.left=new TreeNode(5);
        treeNode.left.right=new TreeNode(6);
        System.out.println(lowestCommonAncestor(treeNode,treeNode.left,treeNode.left.right));
    }


    public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q) { // 递归结束条件
            return root;
        }

        // 后序遍历
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);

        if(left == null && right == null) { // 若未找到节点 p 或 q
            return null;
        }else if(left == null && right != null) { // 若找到一个节点
            return right;
        }else if(left != null && right == null) { // 若找到一个节点
            return left;
        }else { // 若找到两个节点
            return root;
        }
    }
}
