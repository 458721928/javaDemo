package com.javaDemo.ti;

import java.util.HashSet;
import java.util.Set;

/**
 * @ClassName: Kuaileshu
 * @Auther: csy
 * @Date: 2021/1/6 11:38
 * 202. 快乐数
 * 编写一个算法来判断一个数是不是“快乐数”。
 * 一个“快乐数”定义为：对于一个正整数，每一次将该数替换为它每个位置上的数字的平方和，
 * 然后重复这个过程直到这个数变为 1，也可能是无限循环但始终变不到 1。如果可以变为 1，那么这个数就是快乐数。
 * <p>
 * 示例:
 * 输入: 19
 * 输出: true
 * 解释:
 * 1^2 + 9^2 = 82
 * 8^2 + 2^2 = 68
 * 6^2 + 8^2 = 100
 * 1^2 + 0^2 + 0^2 = 1
 * <p>
 * 分析：
 * 无限循环按照题目所给的方法计算，当计算结果为1时返回true,
 * 返回false的条件是计算结果为4，因为只要陷入4 → 16 → 37 → 58 → 89 → 145 → 42 → 20 → 4这个循环，
 * 就为不快乐数。
 * @Description:
 */
public class Kuaileshu {
    public static void main(String[] args) {
        int n = 119;
        // System.out.println(13/10);
        // System.out.println(10/13);
        // System.out.println(13%10);
        // System.out.println(10%13);
        System.out.println(isHappy(n));
    }
    public static boolean isHappy(int n) {
        Set<Integer> seen = new HashSet<>();
        while (n != 1 && !seen.contains(n)) {
            seen.add(n);
            n = getNext(n);
        }
        return n == 1;
    }
    private static int getNext(int n) {
        int totalSum = 0;
        while (n > 0) {
            int d = n % 10;
            n = n / 10;
            totalSum += d * d;
        }
        return totalSum;
    }

}
