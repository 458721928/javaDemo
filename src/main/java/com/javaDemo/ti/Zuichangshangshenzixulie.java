package com.javaDemo.ti;

import java.util.Arrays;

/**
 * @ClassName: Zuichangshangshenzixulie
 * @Auther: csy https://leetcode-cn.com/problems/longest-increasing-subsequence/solution/zui-chang-shang-sheng-zi-xu-lie-dong-tai-gui-hua-2/
 * @Date: 2021/11/26 18:09
 * @Description:
 */
public class Zuichangshangshenzixulie {
    // Dynamic programming.
        public static int lengthOfLIS(int[] nums) {
            if(nums.length == 0) return 0;
            int[] dp = new int[nums.length];
            int res = 0;
            Arrays.fill(dp, 1);
            for(int i = 0; i < nums.length; i++) {
                for(int j = 0; j < i; j++) {
                    if(nums[j] < nums[i]) {
                        dp[i] = Math.max(dp[i], dp[j] + 1);
                    }
                }
                res = Math.max(res, dp[i]);
            }
            return res;
        }

    public static void main(String[] args) {
        int[] a={0,1,0,3,2,3};
        System.out.println(lengthOfLIS(a));
    }
}
