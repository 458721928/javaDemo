package com.javaDemo.ti;

public class DoubleNode {

    //数据
    private Integer data;
    //后续节点节点
    private DoubleNode next;
    //前驱节点
    private DoubleNode previous;
    // 省略set get


    public Integer getData() {
        return data;
    }

    public void setData(Integer data) {
        this.data = data;
    }

    public DoubleNode getNext() {
        return next;
    }

    public void setNext(DoubleNode next) {
        this.next = next;
    }

    public DoubleNode getPrevious() {
        return previous;
    }

    public void setPrevious(DoubleNode previous) {
        this.previous = previous;
    }
}