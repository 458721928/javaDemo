package com.javaDemo.ti;

/**
 * @ClassName: Throw
 * @Auther: csy
 * @Date: 2021/4/15 15:42
 * @Description:
 */
public class Throw {
    public static void main(String[] args) {
        long size=1100;
        int TIME_SIZE=1000;
        Long times = size % TIME_SIZE == 0 ? size / TIME_SIZE : size / TIME_SIZE + 1;
        System.out.println(times);
        // try {
        //     methodA();
        // } catch (ArrayIndexOutOfBoundsException e) {
        //     e.printStackTrace();
        // }
    }

    private static void methodA() {
        try {
            methodB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void methodB() {
        throw new ArrayIndexOutOfBoundsException();
    }
}
