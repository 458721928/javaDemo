package com.javaDemo.ti;

/**
 * 整数翻转
 */
public class Zhengshufanzhuan {

    public static void main(String[] args) {
        int x = 123;
        int y = 0;
        while (x != 0) {
            if (y < Integer.MIN_VALUE / 10 || y > Integer.MAX_VALUE / 10) {
                System.out.println(0);
            }
            int digit = x % 10;
            x /= 10;
            y = y * 10 + digit;
        }
        System.out.println(y);
    }
    
}
