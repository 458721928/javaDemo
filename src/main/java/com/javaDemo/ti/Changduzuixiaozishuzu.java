package com.javaDemo.ti;

/**
 * @ClassName: Changduzuixiaozishuzu
 * @Auther: csy  https://www.programmercarl.com/0209.%E9%95%BF%E5%BA%A6%E6%9C%80%E5%B0%8F%E7%9A%84%E5%AD%90%E6%95%B0%E7%BB%84.html#%E6%9A%B4%E5%8A%9B%E8%A7%A3%E6%B3%95
 * @Date: 2022/7/26 15:53
 * @Description:
 */
public class Changduzuixiaozishuzu {
    public static void main(String[] args) {
        int[] nums={2,3,1,2,4,3};
        System.out.println(minSubArrayLen(7,nums));
    }
    // 滑动窗口
    public static int minSubArrayLen(int s, int[] nums) {
        int left = 0;
        int sum = 0;
        int result = Integer.MAX_VALUE;
        for (int right = 0; right < nums.length; right++) {
            sum += nums[right];
            while (sum >= s) {
                result = Math.min(result, right - left + 1);
                sum -= nums[left++];
            }
        }
        return result == Integer.MAX_VALUE ? 0 : result;
    }
}
