package com.javaDemo.ti;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @ClassName: Huisuzuhe
 * @Auther: csy
 * @Date: 2021/6/24 16:08
 * @Description:
 */
public class Huisuzuhe {
    static  List<Queue<Integer>>result=new ArrayList<>();
    public static void main(String[] args) {
        int[] a={4,2};
        Queue queue=new LinkedList<>();
        dfs(a,queue,a.length);
        System.out.println(1);
    }

    private static void dfs(int[] a, Queue queue, int length) {
        if(queue.size()==length){
            Queue q=new LinkedList<>(queue);
            result.add(q);
            return;
        }
        for(int entity:a){
            queue.add(entity);
            dfs(a,queue,length);
            queue.remove();
        }
    }


}
