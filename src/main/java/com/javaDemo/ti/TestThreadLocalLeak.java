package com.javaDemo.ti;

public class TestThreadLocalLeak {
    static ThreadLocal LOCAL = new ThreadLocal();

    public static void main(String[] args) {
        LOCAL.set("测试ThreadLocalMap弱引用自动回收");
        Thread thread = Thread.currentThread();
        LOCAL = null;
        System.gc();
        System.out.println("");
    }
}