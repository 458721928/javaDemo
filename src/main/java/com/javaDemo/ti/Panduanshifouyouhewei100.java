package com.javaDemo.ti;

/**
 * @ClassName: Panduanshifouyouhewei100
 * @Auther: csy
 * @Date: 2021/6/18 15:56
 * @Description:
 */
public class Panduanshifouyouhewei100 {

    public static void main(String[] args) {
        int[] a={12,13,14,49,51};
        System.out.println(findSum(a,100));
    }

    // 判断是否有和为n的组合，动规法，O(n^2)
    public static boolean findSum(int[] a, int n) {
        boolean[] dp = new boolean[n + 1];
        for (int i = 0; i < a.length; i++) {
            if (a[i] > n) {
                continue;
            }
            for (int j = n; j >= a[i]; j--) {
                if (dp[j - a[i]]) {
                    dp[j] = true;
                }
            }
            dp[a[i]] = true;
            if (dp[n]) {
                return true;
            }
        }
        return false;
    }
}
