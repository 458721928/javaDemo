package com.javaDemo.ti;

/**
 * @ClassName: Yanghuisanjiao
 * @Auther: csy
 * @Date: 2022/9/26 02:57
 * @Description:
 */
public class Yanghuisanjiao {

        // public static void main(String[] args) {
        //     // 声明部分（三角形二维数组）
        //     int[][] a = new int[15][];
        //     // 每行元素个数跟行数一致
        //     for (int i = 0; i < a.length; i++) {
        //         a[i] = new int[i + 1];
        //     }
        //
        //     // 边界赋值
        //     for (int i = 0; i < a.length; i++) {
        //         a[i][0] = 1; // 左边界
        //         a[i][i] = 1; // 右边界（对角线）
        //     }
        //     // 内部元素采用递推公式计算
        //     for (int i = 2; i < a.length; i++) {
        //         for (int j = 1; j < i; j++) {
        //             a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
        //         }
        //     }
        //
        //     // 输出杨辉三角形
        //     for (int i = 0; i < a.length; i++) {
        //         for (int j = 0; j <= i; j++) {
        //             System.out.print(String.format("%-5d", a[i][j]));
        //         }
        //         System.out.println();
        //     }
        // }
        //


        public static void main(String[] args) {
            //读取杨辉三角行数n
            System.out.println("请输入需要打印的杨辉三角行数:");
            int n = 5;
            //创建二维数组
            int[][] arr=new int[n+1][n+1];
            //依据杨辉三角特性赋值二维数组
            for (int i=0;i<n;i++)
                for (int j=1;j<=i;j++){
                    if (j==0||i==j)
                        arr[i][j]=1;
                    else {
                        arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
                    }
                }
            //输出杨辉三角(等腰三角形)
            int k=n;
            for (int i=0;i<n;i++) {
                for (int m=0;m<k-i;m++){
                    System.out.print(" ");
                }
                for(int j=0;j<=i;j++) {
                    if (arr[i][j]!=0)
                        System.out.print(arr[i][j]+" ");
                }
                System.out.println();
            }

    }

}
