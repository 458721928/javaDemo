package com.javaDemo.ti;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: Dianhuahaomazuhe
 * @Auther: csy
 * @Date: 2021/3/13 14:24
 * @Description:
 */
public class Dianhuahaomazuhe {
    static Map<Character,char[]> map=new HashMap<>();
    static {
        map.put('2', new char[]{'a', 'b', 'c'});
        map.put('3', new char[]{'d', 'e', 'f'});
        map.put('4', new char[]{'g', 'h', 'i'});
        map.put('5', new char[]{'j', 'k', 'l'});
        map.put('6', new char[]{'m', 'n', 'o'});
        map.put('7', new char[]{'p', 'q', 'r','s'});
        map.put('8', new char[]{'t', 'u', 'v'});
        map.put('9', new char[]{'w', 'x', 'y','z'});
    }

    public static void main(String[] args) {
        List<String> result=new ArrayList<>();
        String a="23";
        getStr(result,a,0,new StringBuffer());
        System.out.println(result);
    }

    private static void getStr(List<String> result, String a, int index,StringBuffer stringBuffer) {
        if(index==a.length()){
             result.add(stringBuffer.toString());
        }else{
            char aone=a.charAt(index);
            char[] mone=map.get(aone);
            for(int i=0;i<mone.length;i++){
                stringBuffer.append(mone[i]);
                getStr(result,a,index+1,stringBuffer);
                stringBuffer.deleteCharAt(index);
            }
        }
    }

}
