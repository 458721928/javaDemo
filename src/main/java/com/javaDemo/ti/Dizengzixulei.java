package com.javaDemo.ti;

/**
 * @ClassName: Dizengzixulei
 * @Auther: csy
 * @Date: 2021/12/7 15:45
 * @Description:
 */
public class Dizengzixulei {
    public static void main(String[] args) {
        int[] nums={10,9,2,5,3,7,101,18};
        int[] dp=new int[nums.length];
        int count=0;
        for(int i=0;i<nums.length-1;i++){
            for(int j=i+1;j<nums.length;j++){
                if(nums[j]>=nums[i]){
                    dp[j]= Math.max(dp[j],dp[i]+1);
                }
                count = Math.max(count, dp[j]);
            }
        }
        System.out.println(count);
    }
}
