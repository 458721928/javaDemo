package com.javaDemo.ti;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: Intreverse
 * @Auther: csy
 * @Date: 2020/11/22 15:40
 * @Description:
 */
public class Intreverse {
    public static void main(String[] args) {
        int result=0;
        int x=234;
        //1.将整数的字符提取出来
        int quotient;
        //余数
        int remainder;
        List<Integer> remainders= new ArrayList<>();
        do {
            quotient = x / 10;
            remainder = x % 10;
            x = quotient;
            remainders.add(remainder);
        } while (quotient > 0);
        int size = remainders.size();
        for(int i : remainders){
            size =size-1;
            result = result+ i*pow(size);
        }
        System.out.println(result);
    }

    public static int pow(int x) {
        int result =1;
        for(int i = 0;i<x;i++){
            result = result*10;
        }
        return result;
    }
}
