package com.javaDemo.ti;

/**
 * 回文数
 * https://leetcode-cn.com/problems/palindrome-number/
 */
public class HuiWenShu {
    public static void main(String[] args) {
        System.out.println(isPalindrome(1234321));
    }

    public static boolean isPalindrome(int x) {
        if (x < 0 || (x % 10 == 0 && x != 0)) {
            return false;
        }
        int y = 0;
        while(x > y) {
            y = y * 10 + x %10;
            x = x / 10;
        }
        return x == y || x == y /10;
    }
}
