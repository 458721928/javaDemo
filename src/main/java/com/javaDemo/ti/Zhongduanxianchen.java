package com.javaDemo.ti;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

class Zhongduanxianchen extends Thread {
    volatile boolean stop = false;// 线程中断信号量

    // public static void main(String args[]) throws Exception {
    //     long start=System.currentTimeMillis();
    //     System.out.println(System.currentTimeMillis());
    //     Zhongduanxianchen thread = new Zhongduanxianchen();
    //     System.out.println("Starting thread...");
    //     thread.start();
    //     Thread.sleep(900);
    //     System.out.println("Asking thread to stop...");
    //     // 设置中断信号量
    //     thread.stop = true;
    //     Thread.sleep(3000);
    //     System.out.println("Stopping application...");
    //     System.out.println("count"+(System.currentTimeMillis() - start));
    // }

    public void run() {
        long start=System.currentTimeMillis();
        // 每隔一秒检测一下中断信号量
        while (!stop) {
            System.out.println("Thread is running...");
            long time = System.currentTimeMillis();
            /*
             * 使用while循环模拟 sleep 方法，这里不要使用sleep，否则在阻塞时会 抛
             * InterruptedException异常而退出循环，这样while检测stop条件就不会执行，
             * 失去了意义。
             */
            while ((System.currentTimeMillis() - time < 500)) {
                System.out.println(System.currentTimeMillis() - time);
            }
        }
        System.out.println("child"+(System.currentTimeMillis() - start));
        System.out.println("Thread exiting under request...");
    }
    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();
        FutureTask task = new FutureTask(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, 5);
        FutureTask task2 = new FutureTask(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, 2);
        Future<?> future = executor.submit(task);
        Future<?> future2 = executor.submit(task2);
        List<Future<?>> futures = new ArrayList<Future<?>>();
        futures.add(future);
        futures.add(future2);
        try {
            if (executor.awaitTermination(3, TimeUnit.SECONDS)) {
                System.out.println("task finished");
            } else {
                System.out.println("task time out,will terminate");
                for (Future<?> f : futures) {
                    if (!f.isDone()) {
                        f.cancel(true);
                    }
                }
            }
        } catch (InterruptedException e) {
            System.out.println("executor is interrupted");
        } finally {
            executor.shutdown();
        }
    }
}