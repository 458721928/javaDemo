package com.javaDemo.ti;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName: Geijisuanshijiakuohao
 * @Auther: csy https://leetcode-cn.com/problems/different-ways-to-add-parentheses/solution/java-jian-dan-fen-zhi-tong-su-yi-dong-by-2ykx/
 * @Date: 2021/7/7 16:47
 * @Description:
 */
public class Geijisuanshijiakuohao {
    public static void main(String[] args) {
        String a = "2*3-4";
        System.out.println(diffWaysToCompute(a));
    }

    public static List<Integer> diffWaysToCompute(String input) {
        List list=new LinkedList();
        if (input == null || input.length() <= 0) {
            return new ArrayList<Integer>();
        }
        ArrayList<Integer> curRes = new ArrayList<Integer>();
        int length = input.length();
        char[] charArray = input.toCharArray();
        for (int i = 0; i < length; i++) {
            char aChar = charArray[i];
            if (aChar == '+' || aChar == '-' || aChar == '*') { // 当前字符为 操作符
                List<Integer> leftList = diffWaysToCompute(input.substring(0, i));
                List<Integer> rightList = diffWaysToCompute(input.substring(i + 1));
                for (int leftNum : leftList) {
                    for (int rightNum : rightList) {
                        if (aChar == '+') {
                            curRes.add(leftNum + rightNum);
                        } else if (aChar == '-') {
                            curRes.add(leftNum - rightNum);
                        } else {
                            curRes.add(leftNum * rightNum);
                        }
                    }
                }
            }
        }
        if (curRes.isEmpty()) {
            curRes.add(Integer.valueOf(input));
        }
        return curRes;
    }

}
