package com.javaDemo.ti;

/**
 * @ClassName: Pow
 * @Auther: csy https://leetcode-cn.com/problems/powx-n/solution/powx-n-by-leetcode-solution/
 * @Date: 2022/2/8 18:37
 * @Description:
 */
public class Pow {
    public static void main(String[] args) {
        System.out.println(myPow(2,-2));
    }
    public static double myPow(double x, int n) {
        long N = n;
        return N >= 0 ? quickMul(x, N) : 1.0 / quickMul(x, -N);
    }

    public static double quickMul(double x, long N) {
        if (N == 0) {
            return 1.0;
        }
        double y = quickMul(x, N / 2);
        return N % 2 == 0 ? y * y : y * y * x;
    }
}
