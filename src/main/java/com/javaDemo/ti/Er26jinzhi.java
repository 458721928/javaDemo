package com.javaDemo.ti;

/**
 * @ClassName: Er26jinzhi
 * @Auther: csy
 * @Date: 2021/6/27 10:18
 * @Description:
 */
public class Er26jinzhi {
    public static void main(String[] args) {
        String a=convertToTitle(1);
        System.out.println();
    }
        public static String convertToTitle(int n) {
            int num[]=new int[100];//存26进制结果
            int i = 0;//num的角标
            int len; //num的长度
            String ans = null;
            char a;

            //化成26进制的int数组（倒序的）
            while( n > 0 ){
                if(n % 26!=0){
                    num[i] = n % 26;
                    n = n / 26;
                } else{ //如果可以整除26，整除后结果不应该是0，应该是26的Z。同时少进位。
                    num[i] = 26;
                    n = n / 26 - 1;
                }
                i++;
            }
            len = i;
            //倒序给结果string赋值。为何不能直接把字符付给ans[i]呢？
            for(int j = 0; j < len; j++){
                a = (char) (num[i - 1] + 'A' - 1);
                ans = ans+String.valueOf(a);
                i--;
            }
            return ans;
        }
}
