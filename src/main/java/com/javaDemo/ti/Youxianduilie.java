package com.javaDemo.ti;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @ClassName: Youxianduilie
 * @Auther: csy
 * @Date: 2021/3/12 14:55
 * @Description:
 */
public class Youxianduilie {
    static Comparator<Integer> down=new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
            return o2-o1;
        }

    };

    public static void main(String[] args) {
        Queue<Integer> queue=new PriorityQueue(down);
        queue.add(2) ;
        queue.add(4) ;
        queue.add(5) ;
        while(!queue.isEmpty())
        {
            System.out.print(queue.poll()+" ");
        }
    }

}
