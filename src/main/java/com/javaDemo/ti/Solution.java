package com.javaDemo.ti;

class Solution {
    public static int singleNumber(int[] nums) {
        int single = 0;
        for (int num : nums) {
            single ^= num;
        }
        return single;
    }

    public static void main(String[] args) {
        int[] a={1,1,3,3,6,5,5};
        System.out.println(singleNumber(a));
    }
}