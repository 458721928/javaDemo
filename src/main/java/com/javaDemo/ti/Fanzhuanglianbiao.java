package com.javaDemo.ti;

/**
 * @ClassName: Fanzhuanglianbiao
 * @Auther: csy
 * @Date: 2021/3/11 19:18
 * @Description:
 */
public class Fanzhuanglianbiao {
    public static void main(String[] args) {
        ListNode listNode=new ListNode(1);
        listNode.next=new ListNode(4);
        listNode.next.next=new ListNode(3);
        reverse(listNode);
    }

    private static void reverse(ListNode listNode) {
        ListNode current=listNode;
        ListNode newNode=null;
        ListNode nextNode=null;
        while(current!=null){

            nextNode=current.next;

            current.next=newNode;

            newNode=current;

            current=nextNode;

        }
        System.out.println(newNode);
    }



    static class ListNode{
        int val;
        ListNode next;
        public ListNode(int x){
            val=x;
        }

    }
}




