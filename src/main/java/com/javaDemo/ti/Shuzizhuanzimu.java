package com.javaDemo.ti;

/**
 * @ClassName: Shuzizhuanzimu
 * @Auther: csy 数字转26进制
 * @Date: 2021/6/25 15:43
 * @Description:
 */
public class Shuzizhuanzimu {
    public String numToLetter(Integer input,int length) {
        String rS = "";
        int index = 1;
        int count = 0;
        do{
            index = input/(int)Math.pow(26,count);
            count++;
        }while (index>0);
        int remainder = input;
        for(int i = count-2;i>=0;i--){
            int pow = (int)Math.pow(26,i);//种子
            index = remainder/pow;//商
            remainder%=pow;//余数
            rS += (char)((byte)(index)+65);
        }
        for(;count<=length;count++)//补位
        {
            rS = 'A'+rS;
        }
        return rS;
    }
}
