package com.javaDemo.ti;

/**
 * @ClassName: Plusone
 * @Auther: csy  https://www.cnblogs.com/xiaochuan94/p/9874674.html
 * @Date: 2020/6/7 18:37
 * @Description:
 */
public class Plusone {
    static class Solution {
        public static int[] plusOne(int[] digits) {
            for (int i = digits.length - 1; i >= 0; i--) {
                digits[i]++;
                digits[i] = digits[i] % 10;
                if (digits[i] != 0) {
                    return digits;
                }
            }
            digits = new int[digits.length + 1];
            digits[0] = 1;
            return digits;
        }
    }

    public static void main(String[] args) {
        int[] a={2,8};
        System.out.println(Solution.plusOne(a));
    }
}
