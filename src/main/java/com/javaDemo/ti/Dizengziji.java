package com.javaDemo.ti;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csy
 * @Date: 2022/4/24 20:03
 * @Description: https://leetcode-cn.com/problems/increasing-subsequences/
 */
public class Dizengziji {
    private static List<Integer> path = new ArrayList<>();
    private static List<List<Integer>> res = new ArrayList<>();
    public static void main(String[] args) {
        int[] a=new int[]{4, 6, 7, 7};
        findSubsequences(a);
    }
    public static  List<List<Integer>> findSubsequences(int[] nums) {
        backtracking(nums,0);
        return res;
    }
    private static void backtracking (int[] nums, int start) {
        if (path.size() > 1) {
            res.add(new ArrayList<>(path));
        }

        int[] used = new int[201];
        for (int i = start; i < nums.length; i++) {
            if (!path.isEmpty() && nums[i] < path.get(path.size() - 1) || (used[nums[i] + 100] == 1)) {
                continue;
            }
            used[nums[i] + 100] = 1;
            path.add(nums[i]);
            backtracking(nums, i + 1);
            path.remove(path.size() - 1);
        }
    }
}
