package com.javaDemo.ti;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: BianLiyicizhaochuxiiangjia
 * @Auther: csy
 * @Date: 2021/3/17 09:52
 * @Description:
 */
public class BianLiyicizhaochuxiiangjia {
    static Integer[] ints = {1, 2, 2, 1, 4, 5};

    public static void main(String[] args) {
        int a = 7;
        int[] result = getCount(a);
        System.out.println(result.toString());
    }

    private static int[] getCount(int a) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < ints.length; i++) {
            if (map.containsKey(a - ints[i])) {
                return new int[]{map.get(a - ints[i]), i};
            }
            map.put(ints[i], i);
        }
        return new int[0];
    }
}
