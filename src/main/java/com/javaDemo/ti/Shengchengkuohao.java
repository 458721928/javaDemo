package com.javaDemo.ti;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: Shengchengkuohao
 * @Auther: csy https://leetcode-cn.com/problems/generate-parentheses/
 * @Date: 2021/2/23 11:46
 * @Description:
 */
public class Shengchengkuohao {
    static List<String> result=new ArrayList<>();

    public static void main(String[] args) {
        int a=3;
        generateParenthesis(a);
    }

    public static List<String> generateParenthesis(int n) {
        List<String> ans = new ArrayList<String>();
        backtrack(ans, new StringBuilder(), 0, 0, n);
        return ans;
    }

    public static void backtrack(List<String> ans, StringBuilder cur, int left, int right, int max) {
        if (cur.length() == max * 2) {
            ans.add(cur.toString());
            return;
        }
            if (left < max) {
            cur.append('(');
            backtrack(ans, cur, left + 1, right, max);
            cur.deleteCharAt(cur.length() - 1);
        }
        if (right < left) {
            cur.append(')');
            backtrack(ans, cur, left, right + 1, max);
            cur.deleteCharAt(cur.length() - 1);
        }
    }

}
