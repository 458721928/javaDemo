package com.javaDemo.ti;

/**
 * @ClassName: Zuichangzuidazichuan
 * @Auther: csy https://leetcode-cn.com/problems/lian-xu-zi-shu-zu-de-zui-da-he-lcof/solution/dong-tai-gui-hua-li-yong-bian-liang-cun-xg5c2/
 * @Date: 2021/3/6 16:57
 * @Description:
 */
public class Zuichangzuidazichuan {
    public static void main(String[] args) {
        int a[]={-2,-5,6};
        System.out.println(maxSubArray(a));
    }
    public static int maxSubArray(int[] nums) {
        int dp = nums[0], res = dp;
        for(int i = 1; i < nums.length; i++) {
            dp = dp <= 0 ? nums[i] : dp + nums[i];
            res = Math.max(res, dp);
        }
        return res;
    }

}
