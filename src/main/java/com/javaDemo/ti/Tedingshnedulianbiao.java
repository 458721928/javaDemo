package com.javaDemo.ti;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @ClassName: Tedingshnedulianbiao
 * <p>
 * https://leetcode-cn.com/problems/list-of-depth-lcci/solution/te-ding-shen-du-jie-dian-lian-biao-an-ceng-ban-li-/
 * @Auther: csy
 * @Date: 2020/5/27 22:00
 * @Description:
 */
public class Tedingshnedulianbiao {
    public static void main(String[] args) {
        TreeNode treeNode=new TreeNode(1);
        TreeNode treeNode2=new TreeNode(2);
        TreeNode treeNode3=new TreeNode(3);
        TreeNode treeNode4=new TreeNode(4);
        TreeNode treeNode5=new TreeNode(5);
        TreeNode treeNode7=new TreeNode(7);
        TreeNode treeNode8=new TreeNode(8);
        treeNode.left=treeNode2;
        treeNode.right=treeNode3;
        treeNode2.left=treeNode4;
        treeNode2.right=treeNode5;
        treeNode3.right=treeNode7;
        treeNode4.left=treeNode8;
        System.out.println(listOfDepth(treeNode));
    }
    //特定深度节点链表
    public static ListNode[] listOfDepth(TreeNode tree) {
        if (tree == null) return null;
        List<ListNode> resArr = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(tree);
        while (!queue.isEmpty()) {
            int size = queue.size();
            ListNode node = new ListNode(0);//头节点
            ListNode head = node;
            for (int i = 0; i < size; i++) {
                TreeNode p = queue.poll();
                ListNode n = new ListNode(p.val);
                node.next = n;
                node = n;
                if (p.left != null) {
                    queue.add(p.left);
                }
                if (p.right != null) {
                    queue.add(p.right);
                }
            }
            resArr.add(head.next);
        }
        return resArr.toArray(new ListNode[resArr.size()]);
    }

    public static class  TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

}
