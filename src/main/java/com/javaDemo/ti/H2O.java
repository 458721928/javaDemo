package com.javaDemo.ti;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

class H2O {

    volatile int hNum = 0;

    ReentrantLock lock = new ReentrantLock();
    // O元素 满足条件的队列
    Condition oFull = lock.newCondition();
    // H元素 满足条件的队列
    Condition hFull = lock.newCondition();

    public H2O() {
        
    }

    public void hydrogen(Runnable releaseHydrogen) throws InterruptedException {
        lock.lock();
        try {
            while (hNum == 2) {
                hFull.signal();
                oFull.await();
            }
            releaseHydrogen.run();
            hNum++;
            // 避免最后都是H元素，O元素的线程一直在wait
            if (hNum == 2) {
                hFull.signal();
            }
        } finally {
            lock.unlock();
        }
        
    }

    public void oxygen(Runnable releaseOxygen) throws InterruptedException {
        lock.lock();
        try {
            while (hNum != 2) {
                hFull.await();
            }
            releaseOxygen.run();
            hNum = 0;
            oFull.signalAll();
        } finally {
            lock.unlock();
        }
        
    }
    public static void main(String[] args) throws Exception {
        Thread thread = Thread.currentThread();
        System.out.println("再次中断之前interrupted():" + Thread.interrupted());
        // 再次调用interrupt方法中断自己，将中断状态设置为“中断”
        synchronized (thread) {
            thread.interrupt();
            Thread.sleep(100);
            System.out.println("再次中断之前interrupted():" + Thread.interrupted());
            System.out.println("后interrupted():" + Thread.interrupted());
        }

        // method();

        // System.out.println("再次interrupt()后isInterrupted():" + thread.isInterrupted());
        // System.out.println("再次interrupt()后第一次interrupted()返回:" + Thread.interrupted());// clear status
        // // interrupted()判断是否中断，还会清除中断标志位
        // System.out.println("interrupted()后此时再判断IsInterrupted: " + thread.isInterrupted());
        // System.out.println("---------After Interrupt Status Cleared----------");
        // System.out.println("再次interrupt()后第二次interrupted()返回: " + Thread.interrupted());
        // System.out.println("此时再判断IsInterrupted: " + thread.isInterrupted());
        //
        // System.out.println("Main thread stopped.");
    }

    public static synchronized void method(){
        //然后调用：Exchanger.class.notify()...
        H2O.class.notify();
        System.out.println("aaa");
    }

    static class ThreadA extends Thread{
        int count = 0;
        @Override
        public void run(){
            System.out.println(getName() + " 将要运行...");
            while (!this.isInterrupted()){
                System.out.println(getName() + " 运行中 " + count++);
                try{
                    Thread.sleep(400);   // 休眠400毫秒
                }catch(InterruptedException e){  // 退出阻塞态时将捕获异常
                    System.out.println(getName()+"从阻塞态中退出...");
                    this.interrupt();  // 改变线程状态，使循环结束
                }
            }
            System.out.println(getName() + " 已经终止！");
        }
    }

    // public static void main(String argv[]) throws InterruptedException{
    //     ThreadA ta = new ThreadA();
    //     ta.setName("ThreadA");
    //     ta.start();
    //     Thread.sleep(2000);// 主线程休眠2000毫秒，等待其他线程执行
    //     System.out.println(ta.getName()+" 正在被中断.....");
    //     ta.interrupt();  //  中断线程ThreadA
    // }
}
