package com.javaDemo.ti;

/**
 * @ClassName: Huisufa
 * https://leetcode-cn.com/problems/word-search/solution/zai-er-wei-ping-mian-shang-shi-yong-hui-su-fa-pyth/
 * @Auther: csy
 * @Date: 2020/6/7 17:13
 * @Description:
 */
public class Huisufa {
    public static void main(String[] args) {
         char[][] a={ {'A','B','C','E'}, {'S','F','C','S'}, {'A','D','E','E'}};
         String word="SEE";
        System.out.println(exist(board,word));
    }
    private static final int[][] DIRECTIONS = {{-1, 0}, {0, -1}, {0, 1}, {1, 0}};
    private static int rows;
    private static int cols;
    private static int len;
    private static boolean[][] visited;
    private static char[] charArray;
    private static char[][] board={ {'A','B','C','E'}, {'S','F','C','S'}, {'A','D','E','E'}};

    public static boolean exist(char[][] board, String word) {
        rows = board.length;
        if (rows == 0) {
            return false;
        }
        cols = board[0].length;
        visited = new boolean[rows][cols];

        len = word.length();
        charArray = word.toCharArray();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (dfs(i, j, 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean dfs(int x, int y, int begin) {
        if (begin == len - 1) {
            return board[x][y] == charArray[begin];
        }
        if (board[x][y] == charArray[begin]) {
            visited[x][y] = true;
            for (int[] direction : DIRECTIONS) {
                int newX = x + direction[0];
                int newY = y + direction[1];
                if (inArea(newX, newY) && !visited[newX][newY]) {
                    if (dfs(newX, newY, begin + 1)) {
                        return true;
                    }
                }
            }
            visited[x][y] = false;
        }
        return false;
    }

    private static boolean inArea(int x, int y) {
        return x >= 0 && x < rows && y >= 0 && y < cols;
    }

}
