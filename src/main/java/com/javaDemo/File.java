package com.javaDemo;

import lombok.SneakyThrows;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * @ClassName: File
 * @Auther: csy
 * @Date: 2022/2/25 10:26
 * @Description:
 */
public class File {
    @SneakyThrows
    public static void main(String[] args) {
//         FileInputStream fileInputStream = new FileInputStream("/Users/csy/Desktop/C");
//         InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
//         FileOutputStream fileOutputStream = new FileOutputStream(new java.io.File("/Users/csy/Desktop/B"));
//         OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
//         char[] by = new char[1024];
//         int read = 0;
//         while ((read = inputStreamReader.read()) != -1) {
//             System.out.println(read);
//             outputStreamWriter.write(read);
//         }
//         outputStreamWriter.flush();
//         inputStreamReader.close();
//         outputStreamWriter.close();
//
//
// //创建一个读取流对象并和指定文件相关联。
//         FileInputStream fis = new FileInputStream("C:\\Demo\\test.txt");
// //创建字节流对象，用于操作文件。
//         FileOutputStream fos = new FileOutputStream("D:\\Java\\byte_copy_demo.txt");
//
//         byte[] buf = new byte[1024];
//         int len = 0;
// //将读取到的数据存储在字节缓冲区
//         while ((len = fis.read(buf)) != -1) {
//             fos.write(buf);        //将存储在字节缓冲区的数据写入到目的地
//         }
// //关闭流对象。
//         fis.close();

        FileReader fileReader=new FileReader(new java.io.File("/Users/csy/Desktop/C"));
        FileWriter fileWriter=new FileWriter(new java.io.File("/Users/csy/Desktop/B"));
        char[] c=new char[1];
        while (fileReader.read(c)!=-1){
            System.out.println(c);
            fileWriter.write(c);
        }
        fileReader.close();
        fileWriter.close();

        FileInputStream fis=new FileInputStream("/Users/csy/Desktop/C");
        FileOutputStream fos=new FileOutputStream("/Users/csy/Desktop/A",true);
        int buf_size = 50;
        byte[] buffer = new byte[buf_size];
        int len = 0;
        while (-1 != (len = fis.read(buffer, 0, buf_size))) {
            System.out.println(buffer);
            fos.write(buffer, 0, len);
        }
            fis.close();
        fos.close();
    }
}
