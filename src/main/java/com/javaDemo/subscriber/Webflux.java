package com.javaDemo.subscriber;

import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.Random;

/**
 * @ClassName: Webflux
 * @Auther: csy
 * @Date: 2020/7/16 11:58
 * @Description:
 */
public class Webflux {

    public static void main(String[] args) {
        // Flux.range(1, 100).buffer(20).subscribe(System.out::println);
        // Mono.defer(()->{
        //     return Mono.error(new RuntimeException());
        // }).subscribe();
        // Mono.fromCallable(() -> "9999").subscribe(System.out::println);
        final Random random = new Random();
        Flux.generate(ArrayList::new, (list, sink) -> {
            int value = random.nextInt(100);
            list.add(value);
            sink.next(value);
            if (list.size() == 10) {
                sink.complete();
            }
            return list;
        }).subscribe(System.out::println);
    }
}
