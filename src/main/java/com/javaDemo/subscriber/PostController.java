// package com.javaDemo.subscriber;
//
// import lombok.AllArgsConstructor;
// import lombok.Builder;
// import lombok.Data;
// import lombok.NoArgsConstructor;
// import lombok.ToString;
// import org.springframework.data.annotation.CreatedDate;
// import org.springframework.data.annotation.Id;
// import org.springframework.web.bind.annotation.*;
//
// import java.time.LocalDateTime;
//
// @RestController()
// @RequestMapping(value = "/posts")
// class PostController {
//
//     private final PostRepository posts;
//
//     public PostController(PostRepository posts) {
//         this.posts = posts;
//     }
//
//     @GetMapping("")
//     public Flux<Post> all() {
//         return this.posts.findAll();
//     }
//
//     @PostMapping("")
//     public Mono<Post> create(@RequestBody Post post) {
//         return this.posts.save(post);
//     }
//
//     @GetMapping("/{id}")
//     public Mono<Post> get(@PathVariable("id") String id) {
//         return this.posts.findById(id);
//     }
//
//     @PutMapping("/{id}")
//     public Mono<Post> update(@PathVariable("id") String id, @RequestBody Post post) {
//         return this.posts.findById(id)
//           .map(p -> {
//               p.setName(post.getName());
//               p.setAge(post.getAge());
//
//               return p;
//           })
//           .flatMap(p -> this.posts.save(p));
//     }
//
//     @DeleteMapping("/{id}")
//     public Mono<Void> delete(@PathVariable("id") String id) {
//         return this.posts.deleteById(id);
//     }
//
// }
//
// interface PostRepository extends ReactiveMongoRepository<Post, String> {
// }
//
// @Data
// @ToString
// @Builder
// @NoArgsConstructor
// @AllArgsConstructor
// class Post {
//
//     @Id
//     private String id;
//     private String name;
//     private Integer age;
//
//     @CreatedDate
//     private LocalDateTime createdDate;
// }