package com.javaDemo.base;

/**
 * @ClassName: Inner
 * @Auther: csy
 * @Date: 2020/3/16 10:11
 * @Description:
 */
public class Inner {
    private double radius = 0;

    public Inner(double radius) {
        this.radius = radius;
        getDrawInstance().drawSahpe();   //必须先创建成员内部类的对象，再进行访问
    }

    private Draw getDrawInstance() {
        return new Draw();
    }

    class Draw {     //内部类
        public void drawSahpe() {
            System.out.println(radius);  //外部类的private成员
        }
    }
    public static void main(String[] args) {
        try {
            // ClassLoader.getSystemClassLoader().loadClass("com.javaDemo.base.Name");
            Class c = Class.forName("com.javaDemo.base.Name", true, com.javaDemo.base.Name.class.getClassLoader());
            System.out.println("#########-------------结束符------------##########");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
