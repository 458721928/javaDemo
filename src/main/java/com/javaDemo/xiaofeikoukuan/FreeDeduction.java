package com.javaDemo.xiaofeikoukuan;

public class FreeDeduction implements IDeduction {
 
	@Override
	public boolean exec(Card card, Trade trade) {
		// 直接从自由余额中扣除
		card.setFeeMoney(card.getFeeMoney() - trade.getAmount());
		return true;
	}
 
}