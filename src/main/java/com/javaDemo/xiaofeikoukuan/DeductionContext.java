package com.javaDemo.xiaofeikoukuan;

public class DeductionContext {
	// 扣款策略
	private IDeduction deduction = null;

	/**
	 * 构造函数传递策略
	 *
	 * @param deduction
	 */
	public DeductionContext(IDeduction deduction) {
		this.deduction = deduction;
	}

	/**
	 * 执行扣款
	 *
	 * @param card
	 * @param trade
	 * @return
	 */
	public boolean exec(Card card, Trade trade) {
		return this.deduction.exec(card, trade);
	}
}
