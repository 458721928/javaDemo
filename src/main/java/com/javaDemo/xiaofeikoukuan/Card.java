package com.javaDemo.xiaofeikoukuan;

/**
 * 消费扣款使用设计模式  https://blog.csdn.net/en_joker/article/details/82982402
 */
public class Card {
	// IC卡号码
	private String cardNo = "";
	// 卡内的固定交易金额
	private int steadyMoney = 0;
	// 卡内自由交易金额
	private int feeMoney = 0;

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public int getSteadyMoney() {
		return steadyMoney;
	}

	public void setSteadyMoney(int steadyMoney) {
		this.steadyMoney = steadyMoney;
	}

	public int getFeeMoney() {
		return feeMoney;
	}

	public void setFeeMoney(int feeMoney) {
		this.feeMoney = feeMoney;
	}

}