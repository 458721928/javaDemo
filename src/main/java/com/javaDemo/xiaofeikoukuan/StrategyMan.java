package com.javaDemo.xiaofeikoukuan;

public enum StrategyMan {
	SteadyDeduction("com.javaDemo.xiaofeikoukua.SteadyDeduction"),
	FreeDeduction("com.javaDemo.xiaofeikoukua.FreeDeduction");

	String value = "";

	private StrategyMan(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
