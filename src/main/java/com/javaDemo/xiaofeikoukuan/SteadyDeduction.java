package com.javaDemo.xiaofeikoukuan;

public class SteadyDeduction implements IDeduction {

	@Override
	public boolean exec(Card card, Trade trade) {
		// 固定金额和自由金额各扣除50%
		int halfMoney = (int) Math.rint(trade.getAmount() / 2.0);
		card.setFeeMoney(card.getFeeMoney() - halfMoney);
		card.setSteadyMoney(card.getSteadyMoney() - halfMoney);
		return true;
	}
}
