package com.javaDemo;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(JavaDemoApplication.class);
	}
	public static void main(String[] args) {
		// 字符串比较
		String a = "陈哈哈";
		String b = "陈哈哈";

		if (a == b) {// true  a==b
			System.out.println("a==b");
		}
		if (a.equals(b)) {// true  a.equals(b)
			System.out.println("a.equals(b)");
		}

		// StringBuffer 对象比较，由于StringBuffer没有重写Object的equal方法，因此结果出现错误
		StringBuffer c = new StringBuffer("陈哈哈");
		StringBuffer d = new StringBuffer("陈哈哈");

		if (c == d) {// false  c != d
			System.out.println("c == d");
		} else {
			System.out.println("c != d");
		}

		if (c.equals(d)) { // false 调用了Object类的equal方法
			System.out.println("StringBuffer equal true");
		}else {
			System.out.println("StringBuffer equal false");
		}
	}

}
