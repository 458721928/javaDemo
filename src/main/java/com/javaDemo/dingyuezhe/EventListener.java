package com.javaDemo.dingyuezhe;

import org.springframework.context.ApplicationListener;

class EventListener implements ApplicationListener<UserRegisterEvent> {
    @Override
    public void onApplicationEvent(UserRegisterEvent event) {               //发邮件
        System.out.println("正在发送邮件至： " + event.getUser().getEmail());               //发短信
        System.out.println("正在发短信到： " + event.getUser().getName());
    }
}