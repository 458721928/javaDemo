package com.javaDemo.dingyuezhe;

import lombok.Data;

/**
 * @ClassName: User
 * @Auther: csy
 * @Date: 2020/3/30 18:00
 * @Description:
 */
@Data
public class User {
    private String name;

    private String email;

}
