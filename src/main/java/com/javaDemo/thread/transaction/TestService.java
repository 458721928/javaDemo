package com.javaDemo.thread.transaction;

/**
 * Created by liuyachao on 2018/9/3.
 */
public interface TestService {
    int saveUser2(User2 user2);

    User3 getUser3List(User3 user3);

    void threadMethod();
}