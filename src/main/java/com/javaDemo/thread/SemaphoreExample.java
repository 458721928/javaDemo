// package com.javaDemo.thread;
//
//
// import java.util.concurrent.ExecutorService;
// import java.util.concurrent.LinkedBlockingQueue;
// import java.util.concurrent.Semaphore;
// import java.util.concurrent.ThreadFactory;
// import java.util.concurrent.ThreadPoolExecutor;
// import java.util.concurrent.TimeUnit;
//
// public class SemaphoreExample {
//
//     public static void main(String[] args) {
//         final int clientCount = 3;
//         final int totalRequestCount = 10;
//         Semaphore semaphore = new Semaphore(clientCount);
//         ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("distributionExecutor-%d").build();
//         ExecutorService executorService = new ThreadPoolExecutor(5, 200,
//                 0L, TimeUnit.MILLISECONDS,
//                 new LinkedBlockingQueue<Runnable>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());
//         for (int i = 0; i < totalRequestCount; i++) {
//             executorService.execute(()->{
//                 try {
//                     semaphore.acquire();
//                     System.out.print(semaphore.availablePermits() + " ");
//                 } catch (InterruptedException e) {
//                     e.printStackTrace();
//                 } finally {
//                     semaphore.release();
//                 }
//             });
//         }
//         executorService.shutdown();
//     }
//
// }