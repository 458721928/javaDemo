package com.javaDemo.thread;

import java.util.concurrent.ArrayBlockingQueue;

public  class Man {
    public static boolean flag = false;

    public static int num = 0;

    public static void main(String[] args) {
        Man man = new Man();
        new Thread(() -> {
            man.getRunnable1();
        }).start();
        new Thread(() -> {
            man.getRunnable2();
        }).start();
    }

    // public synchronized void getRunnable1() {
    //         for (int i = 0; i < 20; i++) {
    //             while (flag) {
    //                 try {
    //                     wait();
    //                 } catch (InterruptedException e) {
    //                     e.printStackTrace();
    //                 }
    //             }
    //             System.out.println("生产出：" + (++num) + "个");
    //             flag = true;
    //             notify();
    //         }
    //     }
    //
    //     public synchronized void getRunnable2() {
    //         for (int i = 0; i < 20; i++) {
    //             while (!flag) {
    //                 try {
    //                     wait();
    //                 } catch (InterruptedException e) {
    //                     e.printStackTrace();
    //                 }
    //             }
    //
    //             //模拟加载时间
    //             try {
    //                 Thread.sleep(1000);
    //             } catch (InterruptedException e) {
    //                 e.printStackTrace();
    //             }
    //             System.out.println("取出出：" + (num--) + "个");
    //             System.out.println("------------------");
    //
    //             flag = false;
    //             notify();
    //         }
    //     }


    ArrayBlockingQueue queue = new ArrayBlockingQueue<Integer>(64);

    public void getRunnable1() {
        for (int i = 0; i < 8; i++) {
            System.out.println("生产出：" + i + "个");
            try {
                queue.put(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("---------------生产完毕-----------------");
    }

    public void getRunnable2() {

        for (int i = 0; i < 8; i++) {
            try {
                int num = (int) queue.take();
                System.out.println("取出出：" + num);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    }