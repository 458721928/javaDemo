package com.javaDemo.delayQueue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.DelayQueue;

/**
 * @ClassName: test
 * @Auther: csy
 * @Date: 2020/5/23 21:23
 * @Description:
 */
public class test {

    public static void main(String[] args) {
        DelayQueue<OrderDelay> queue = new DelayQueue<>();
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);

        Date time1 = c.getTime();
        OrderDelay orderDelay1=new OrderDelay();
        orderDelay1.setOrderId(1);
        orderDelay1.setOrderTime(time1);
        queue.put(orderDelay1);
        System.out.println("1： "+ new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(time1));

        c.add(Calendar.DATE, -15);
        Date time2 = c.getTime();
        OrderDelay orderDelay2=new OrderDelay();
        orderDelay2.setOrderId(2);
        orderDelay2.setOrderTime(time2);
        queue.put(orderDelay2);

        System.out.println("2： "+ new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(time2));
        int a=0;


        OrderDelay delay = new OrderDelay();
        delay.setOrderId(123);
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        delay.setOrderTime(currentTime);
        System.out.printf("现在时间是%s;订单%d加入队列%n", dateString, 123);
        queue.put(delay);



    }
}
